﻿using UnityEngine;
using System.Collections;

public class test_EventTarget : MonoBehaviour 
{

    vp_FPPlayerEventHandler m_Player;
    void Awake()
    {
        m_Player = transform.GetComponent<vp_FPPlayerEventHandler>();
    }
    //регистрируемся в ивент системе
    protected virtual void OnEnable()
    {
        if (m_Player != null)
            m_Player.Register(this);
    }
    protected virtual void OnDisable()
    {
        if (m_Player != null)
            m_Player.Unregister(this);
    }

    //event listener
    //actual implementation of the printing method
    void OnMessage_PrintSomething(string s)
    {
        Debug.Log("_" + s);
    }

    bool OnAttempt_Boomass(){

        return true;
    }

    int curExp = 100;

    int OnValue_Exp
    {
        get
        {
            return curExp;
        }
        set 
        {
            curExp = value;
        }
    }

	void Start () 
	{
	
	}
	
	
	void Update () 
	{
	
	}
}
