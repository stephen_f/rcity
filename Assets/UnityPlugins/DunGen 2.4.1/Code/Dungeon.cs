﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using DunGen.Graph;

namespace DunGen
{
	public class Dungeon : MonoBehaviour
	{
		public DungeonFlow DungeonFlow { get; protected set; }

        public ReadOnlyCollection<Tile> AllTiles { get; private set; }
        public ReadOnlyCollection<Tile> MainPathTiles { get; private set; }
        public ReadOnlyCollection<Tile> BranchPathTiles { get; private set; }
        public ReadOnlyCollection<DoorwayConnection> Connections { get; private set; }
        public DungeonGraph ConnectionGraph { get; private set; }

		private readonly List<Tile> allTiles = new List<Tile>();
        private readonly List<Tile> mainPathTiles = new List<Tile>();
        private readonly List<Tile> branchPathTiles = new List<Tile>();
        private readonly List<DoorwayConnection> connections = new List<DoorwayConnection>();


		internal void PreGenerateDungeon(DungeonGenerator dungeonGenerator)
		{
			DungeonFlow = dungeonGenerator.DungeonFlow;

            AllTiles = new ReadOnlyCollection<Tile>(new Tile[0]);
            MainPathTiles = new ReadOnlyCollection<Tile>(new Tile[0]);
            BranchPathTiles = new ReadOnlyCollection<Tile>(new Tile[0]);
            Connections = new ReadOnlyCollection<DoorwayConnection>(new DoorwayConnection[0]);
		}

        internal void PostGenerateDungeon(DungeonGenerator dungeonGenerator)
        {
            ConnectionGraph = new DungeonGraph(this);
        }

		public void Clear()
		{
			foreach(var tile in allTiles)
		        GameObject.DestroyImmediate(tile.gameObject);

			allTiles.Clear();
			mainPathTiles.Clear();
			branchPathTiles.Clear();
            connections.Clear();

            ExposeRoomProperties();
		}

        internal void MakeConnection(Doorway a, Doorway b, System.Random randomStream)
        {
            var conn = new DoorwayConnection(a, b);

            a.Tile.Placement.UnusedDoorways.Remove(a);
            a.Tile.Placement.UsedDoorways.Add(a);

            b.Tile.Placement.UnusedDoorways.Remove(b);
            b.Tile.Placement.UsedDoorways.Add(b);

            connections.Add(conn);

            // Add door prefab
            List<GameObject> doorPrefabs = (a.DoorPrefabs.Count > 0) ? a.DoorPrefabs : b.DoorPrefabs;
            
            if (doorPrefabs.Count > 0)
            {
                GameObject doorPrefab = doorPrefabs[randomStream.Next(0, doorPrefabs.Count)];

                if (doorPrefab != null)
                {
                    GameObject door = (GameObject)GameObject.Instantiate(doorPrefab);
                    door.transform.position = a.transform.position;
                    door.transform.rotation = a.transform.rotation;
                    door.transform.localScale = a.transform.localScale;

                    door.transform.parent = a.transform;

                    a.SetUsedPrefab(door);
                    b.SetUsedPrefab(door);
                }
            }
        }

        internal void AddTile(Tile tile)
        {
            allTiles.Add(tile);

            if (tile.Placement.IsOnMainPath)
                mainPathTiles.Add(tile);
            else
                branchPathTiles.Add(tile);
        }

        internal void ExposeRoomProperties()
        {
            AllTiles = new ReadOnlyCollection<Tile>(allTiles);
            MainPathTiles = new ReadOnlyCollection<Tile>(mainPathTiles);
            BranchPathTiles = new ReadOnlyCollection<Tile>(branchPathTiles);
            Connections = new ReadOnlyCollection<DoorwayConnection>(connections);
        }
	}
}
